# FormValidation

### Project to test validations across the TLN app.
JSON validation specs
* `validator` : The validator to apply. Can be `Required` | `Range` | `Relative` | `Length` | `Uniqueness`
* `keyLabel` : The label to be shown in the message.
* `range` : Used for **range validation only.** It must consists of two keys Metric and Imperial and has min and max values of both the unit systems. It can also have symbols for each unit system if it needs to show along with message. 
* `comparator` : Used for **relative validation only.** It should be either set as `GreaterThanEqualTo` or `LessThanEqualTo`.
* `field` : Used for **relative validation only.** Form field with which the validation must be done.
* `fieldLabel` : **Used for relative validation only.** Relative field label to be shown in message.
* `maxLength` : Used for **length validation only.** Max length to check for a form field.
* `errorMessage` : Used for **uniqueness validation only.** Can give custom error messages. Can also give 1 variable eg.
`Environment with the name {name} already exists in this project.` The value for this is set in the uniqueness validator implementation. 
* `type` : Used for **required validation only.** Can be  `string` | `integer` | `decimal`
* `isDisableCheck` : Used to specify if form field need to validate if field is disabled
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
