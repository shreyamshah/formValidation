import { isNullOrUndefined } from 'util';
import { FormValidation } from './formValidation';
export class Length implements FormValidation {

  constructor(key: string, keyLabel: string, value: any, max: number) {
    this.keyLabel = keyLabel;
    this.value = value;
    this.max = max;
    this.data = `${keyLabel} should not exceed ${max} characters`;
  }
  max: number;
  keyLabel: string;
  value: number;
  key: string;
  data: string;

  validate(): string | null {
    return (!isNullOrUndefined(this.value) && this.value <= this.max) ? null : this.data;
  }
}
