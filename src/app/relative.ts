import { FormValidation } from './formValidation';
export class Relative implements FormValidation {

  constructor(key: string, keyLabel: string, value: any, comparator: string, relativeValue: number, relativeFieldName: string) {
    this.keyLabel = keyLabel;
    this.value = value;
    this.comparator = comparator;
    this.relativeValue = relativeValue;
    this.relativeFieldName = relativeFieldName;
  }
  comparator: string;
  relativeValue: number;
  keyLabel: string;
  value: number;
  key: string;
  data: string;
  relativeFieldName: string;

  validate(): string | null {
    let isValidated: boolean = false;
    var validations = {
      'GreaterThanEqualTo': function (value: number, relativeValue: number): boolean {
        isValidated = +value >= +relativeValue;
        return isValidated;
      },
      "LessThanEqualTo": function (value: number, relativeValue: number): boolean {
        isValidated = +value <= +relativeValue;
        return isValidated;
      }
    }
    isValidated = validations[this.comparator](this.value, this.relativeValue);
    (this.comparator == 'GreaterThanEqualTo') ?
      this.data = `The ${this.keyLabel} field must be greater than or equal to ${this.relativeFieldName} field.` :
      this.data = `The ${this.keyLabel} field must be less than or equal to ${this.relativeFieldName} field.`
    return !isValidated ? this.data : null;
  }
}
