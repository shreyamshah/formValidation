import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-department-component',
  templateUrl: './department-component.component.html',
  styleUrls: ['./department-component.component.css']
})
export class DepartmentComponentComponent implements OnInit {
  form2: FormGroup

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form2 = this.fb.group({
      departmentName: '',
      role:'',
      turnover : '',
      productionQuantity : ''
    })
  }

}
