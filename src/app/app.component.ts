import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { JsonUtil } from 'src/util/json.util';
import { DepartmentComponentComponent } from './department-component/department-component.component';
import { ValidationService } from './validation.service';
import file from './app.validations.json';
import file2 from './department-component/department.validations.json'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'formValidation';
  parentForm: FormGroup;
  @ViewChild(DepartmentComponentComponent, { static: false }) departmentComponentComponent: DepartmentComponentComponent;

  constructor(private fb: FormBuilder, private validationService: ValidationService) {

  }
  ngOnInit(): void {
    this.parentForm = this.fb.group({
      name: '',
      age: '',
      education: '',
      percentage: '',
      productionMade:''
    });
  }

  submit() {

    let jsonUtilObject : JsonUtil = new JsonUtil([file,file2]);
    let validationParsedData = jsonUtilObject.getValidationFileObject();
    //console.log(validationParsedData.age[0].validator);
    let data = this.validationService.validate([this.parentForm,this.departmentComponentComponent.form2],validationParsedData);
    console.log(data);
  }
}
