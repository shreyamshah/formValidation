import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Validations } from 'src/util/json.util';
import { isNullOrUndefined } from 'util';
import { FormValidation } from './formValidation';
import { Length } from './length';
import { Relative } from './relative';
import { Required } from './required';
import { Uniqueness } from './uniqueness';
import {Range} from './range';
@Injectable({
  providedIn: 'root'
})
export class ValidationService {
  constructor() { }

  public validate(formGroups: Array<FormGroup>, validationData: { [key: string]: Validations[] }, id: string = null, entityList: any[] = null, unitSystem: UnitStandard = UnitStandard.Metric) {
    let errors: { [key: string]: string[] } = {};
    formGroups.forEach(formGroup => {
      Object.keys(formGroup.controls).forEach(element => {
        let data: Validations[] = validationData[element];
        if (data != null) {
          data.forEach((i) => {
            let canValidate: boolean = true;
            if (!isNullOrUndefined(i.isDisableCheck) && i.isDisableCheck){
              if (formGroup.get(element).disabled)
                canValidate = false;
            }
            let validator: FormValidation = null;
            var validations = {
              'Required': function () {
                validator = new Required(element, i.keyLabel, formGroup.get(element).value, i.type, i.errorMessage);
              },
              'Range': function () {
                validator = new Range(element, i.keyLabel, formGroup.get(element).value, i.range[unitSystem].min, i.range[unitSystem].max, i.range[unitSystem].unitSymbol);
              },
              'Length': function () {
                validator = new Length(element, i.keyLabel, formGroup.get(element).value.length, i.maxLength);
              },
              'Relative': function () {
                var relativeValue = null;
                formGroups.forEach(value => {
                  if(value.get(i.field)!=null) relativeValue = value.get(i.field).value;
                });
                validator = new Relative(element, i.keyLabel, formGroup.get(element).value, i.comparator, relativeValue, i.fieldLabel);
              },
              'Uniqueness': function () {
                validator = new Uniqueness(element, i.keyLabel, formGroup.get(element).value, entityList, id, i.errorMessage);
              }
            }
            if (canValidate) {
              validations[i.validator]();
              let error = validator.validate();
              if (error != null && errors[element] == null) errors[element] = [error];
            }
          });
        }
      });
    });
    return errors;
  }
}
