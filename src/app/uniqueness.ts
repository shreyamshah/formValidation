import { EntityNameHandlerService } from '@shared/services/entity-name-handler.service';
import { startCase } from 'lodash';
import { isNullOrUndefined } from 'util';
import { FormValidation } from './formValidation';
export class Uniqueness implements FormValidation {
  public entityNameHandlerService: EntityNameHandlerService;
  constructor(key: string, keyLabel: string, value: any, entityList: any[], id: string, errorMessage: string = null) {
    this.keyLabel = keyLabel;
    this.value = value;
    this.entityList = entityList;
    this.id = id;
    this.data = `${keyLabel} already exists`;
    this.errorMessage = errorMessage;
    this.key = startCase(key).replace(" ", "");
  }
  errorMessage: string;
  keyLabel: string;
  value: string;
  key: string;
  data: string;
  entityList: any[];
  id: string

  validate(): string | null {
    if (typeof this.value === "string") {

      var filteredEntityname = this.entityList.filter(entity =>
        entity.Id != this.id && entity[this.key].trim().toLowerCase() == this.value.trim().toLowerCase())
      if (isNullOrUndefined(filteredEntityname) || filteredEntityname.length == 0) {
        return null;
      }
    }
    else if (typeof this.value === "boolean") {
      var filteredEntity = this.entityList.filter(entity => entity[this.key] == true)
      if (!isNullOrUndefined(filteredEntity) && filteredEntity.length >= 1) {
        return null;
      }
    }
    this.errorMessage != null ? this.data = this.errorMessage.replace('{name}', this.value) : '';
    return this.data;
  }
}