import { TestBed } from '@angular/core/testing';
import { Validators } from '@angular/forms';

import { ValidationService } from './validation.service';

describe('EmployeeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ValidationService = TestBed.get(ValidationService);
    expect(service).toBeTruthy();
  });
});
