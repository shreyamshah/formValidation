import { isNullOrUndefined } from 'util';
import { FormValidation } from './formValidation';
export class Required implements FormValidation {

  constructor(key: string, keyLabel: string, value: any, type: string, errorMessage: string) {
    this.keyLabel = keyLabel;
    this.value = value;
    this.type = type;
    this.errorMessage = errorMessage;
  }
  keyLabel: string;
  value: any;
  key: string;
  data: string;
  type: string;
  errorMessage: string;

  validate(): string | null {
    if (!isNullOrUndefined(this.value) && this.value !== '')
      return null;
    var fieldTypes = {
      "decimal": function (keyLabel) {
        return `The ${keyLabel} value must be a decimal.`;
      },
      "integer": function (keyLabel) {
        return `The ${keyLabel} value must be an integer.`;
      }
    }
    this.data = this.errorMessage != null ? this.errorMessage : fieldTypes[this.type](this.keyLabel);
    return this.data;
  }
}