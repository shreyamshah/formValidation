import { isNullOrUndefined } from 'util';
import { FormValidation } from './formValidation';
export class Range implements FormValidation {

  constructor(key: string, keyLabel: string, value: any, min: number, max: number, unitSymbol?: string) {
    this.keyLabel = keyLabel;
    this.value = value;
    this.min = min;
    this.max = max;
    this.unitSymbol = unitSymbol;
    this.data;
  }
  min: number;
  max: number;
  keyLabel: string;
  value: number;
  key: string;
  unitSymbol: string;
  data: string;

  validate(): string | null {
    //this.data = !isNullOrUndefined(this.unitSymbol) ?
      //`The ${this.keyLabel} field must be between ${this.min} ${UnitSymbol[this.unitSymbol]} and ${this.max} ${UnitSymbol[this.unitSymbol]}.` :
      this.data = `The ${this.keyLabel} field must be between ${this.min} and ${this.max}.`;
    return (!isNullOrUndefined(this.value) && this.value >= this.min && this.value <= this.max) ? null : this.data;
  }
}
