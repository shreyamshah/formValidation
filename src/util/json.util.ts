export class JsonUtil {
  files: any[];
  constructor(files: any[]) {
    this.files = files;
  }

  getValidationFileObject(): { [key: string]: Validations[] } {
    let validationObjectModel: { [key: string]: Validations[] } = {};
    this.files.forEach(file => {
      let individualFile: { [key: string]: Validations[] } = JSON.parse(JSON.stringify(file));
      var keys = Object.keys(individualFile);
      keys.forEach(key => {
        if (validationObjectModel[key] == null) {
          validationObjectModel[key] = individualFile[key];
        }
      });
    });
    return validationObjectModel;
  }
}
export class Validations {
  validator: "Required" | "Range" | "Relative" | "Length"|"Uniqueness";
  keyLabel: string;
  range?: Array<MinMax>;
  comparator?: "GreaterThanEqualTo" | "LessThanEqualTo";
  field?: string;
  fieldLabel?: string;
  maxLength?: number;
  errorMessage?:string;
  entity?:string;
  type?: "string" | "integer" | "decimal";
  isDisableCheck?:boolean=true;
}
export class MinMax {
  min: number;
  max: number;
  unitSymbol?:string;
}